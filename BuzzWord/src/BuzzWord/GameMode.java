package BuzzWord;

/**
 * Created by Mendy on 11/24/2016.
 */
public enum GameMode {

    PEOPLE,
    FOOD,
    ANIMALS,
    DICTIONARY
}
